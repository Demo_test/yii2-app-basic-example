<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'sqlite:' . dirname(__DIR__) . '/runtime/yii2basic.sqlite',
    'charset' => 'utf8',
];
